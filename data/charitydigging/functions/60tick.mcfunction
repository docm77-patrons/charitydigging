#persistent effects on armor
effect give @a[nbt={Inventory:[{Slot:103b,id:"minecraft:golden_helmet",Count:1b}]}] minecraft:night_vision 16 0 true
effect give @a[nbt={Inventory:[{Slot:100b,id:"minecraft:golden_boots",Count:1b}]}] minecraft:jump_boost 4 3 true

#summing the number of blocks broken to get a total, but @a only counts online players
scoreboard players set total sum 0
scoreboard players operation total sum += @a breakStone

#persistent rockets and ender pearls for transportation
replaceitem entity @a container.8 minecraft:firework_rocket 64
replaceitem entity @a container.7 minecraft:ender_pearl 16
kill @e[name="Firework Rocket",type=item]
kill @e[name="Ender Pearl",type=item]

#re-run this function every 3 seconds
schedule function charitydigging:60tick 60