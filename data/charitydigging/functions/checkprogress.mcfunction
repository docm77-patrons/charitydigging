#award advancement to players who don't already have it and qualify for it
advancement grant @a[advancements={charitydigging:efficiency=false},scores={breakStone=35..}] only charitydigging:efficiency
advancement grant @a[advancements={charitydigging:hundooo=false},scores={breakStone=77..}] only charitydigging:hundooo
advancement grant @a[advancements={charitydigging:upgrade=false},scores={breakStone=250..}] only charitydigging:upgrade
advancement grant @a[advancements={charitydigging:stoned=false},scores={breakStone=700..}] only charitydigging:stoned
advancement grant @a[advancements={charitydigging:iron_age=false},scores={breakStone=2000..}] only charitydigging:iron_age
advancement grant @a[advancements={charitydigging:iron_man=false},scores={breakStone=5000..}] only charitydigging:iron_man
advancement grant @a[advancements={charitydigging:the_golden_rule=false},scores={breakStone=12000..}] only charitydigging:the_golden_rule
advancement grant @a[advancements={charitydigging:diamond=false},scores={breakStone=30000..}] only charitydigging:diamond
advancement grant @a[advancements={charitydigging:endgame=false},scores={breakStone=77000..}] only charitydigging:endgame
advancement grant @a[advancements={charitydigging:150000=false},scores={breakStone=150000..}] only charitydigging:150000