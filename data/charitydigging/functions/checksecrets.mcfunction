#award advancement to players who qualify for it
advancement grant @a[scores={flightDistance=10000000..}] only charitydigging:fancy_flight
advancement grant @a[scores={timesJumped=7777..}] only charitydigging:hopper
advancement grant @a[scores={secretsFound=77..}] only charitydigging:treasure_hunt