#chat message
tellraw @p ["",{"text":"The ","color":"aqua"},{"text":"Winding Horror","color":"gold"},{"text":" has been revealed in the South!","color":"aqua"}]

#title
title @a times 20 100 0
title @a title ["",{"text":"Dungeon revealed!","color":"red"}]
title @a subtitle ["",{"text":"The ","color":"aqua"},{"text":"Winding Horror","color":"gold"},{"text":" has been found in the South!","color":"aqua"}]

#load marking beacon
setblock -721 66 329 minecraft:structure_block{name:"charitydigging:beacon",mode:"LOAD",posX:-2,posY:-3,posZ:-2}
setblock -721 67 329 minecraft:redstone_block
setblock -721 68 329 minecraft:light_blue_stained_glass_pane