#advancement revoke @a only charitydigging:dungeon1 #revoke for testing
advancement grant @a only charitydigging:dungeon1

#chat message
tellraw @p ["",{"text":"The ","color":"aqua"},{"text":"Winding Horror","color":"gold"},{"text":" has been conquered! It can now be removed.","color":"aqua"}]

#title
title @a times 20 100 0
title @a title ["",{"text":"Dungeon cleared!","color":"red"}]
title @a subtitle ["",{"text":"The ","color":"aqua"},{"text":"Winding Horror","color":"gold"},{"text":" has been conquered!","color":"aqua"}]

#convert structure to stone
fill -751 52 304 -703 40 352 stone replace #charitydigging:dungeon1
fill -751 39 304 -703 27 352 stone replace #charitydigging:dungeon1
fill -751 26 304 -703 14 352 stone replace #charitydigging:dungeon1
fill -751 13 304 -703 13 352 stone replace #charitydigging:dungeon1

#clear the beacon
fill -723 63 331 -719 68 327 air