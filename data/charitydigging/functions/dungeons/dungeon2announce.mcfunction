tellraw @p ["",{"text":"The ","color":"aqua"},{"text":"Verdant Hollows","color":"gold"},{"text":" have been revealed in the West!","color":"aqua"}]

title @a times 20 100 0
title @a title ["",{"text":"Dungeon revealed!","color":"red"}]
title @a subtitle ["",{"text":"The ","color":"aqua"},{"text":"Verdant Hollows","color":"gold"},{"text":" has been found in the West!","color":"aqua"}]

setblock -803 75 193 minecraft:structure_block{name:"charitydigging:beacon",mode:"LOAD",posX:-2,posY:-3,posZ:-2}
setblock -803 76 193 minecraft:redstone_block
setblock -803 77 193 minecraft:lime_stained_glass_pane