tellraw @p ["",{"text":"The ","color":"aqua"},{"text":"Blazing Abyss","color":"gold"},{"text":" has been revealed in the North-East!","color":"aqua"}]

title @a times 20 100 0
title @a title ["",{"text":"Dungeon revealed!","color":"red"}]
title @a subtitle ["",{"text":"The ","color":"aqua"},{"text":"Blazing Abyss","color":"gold"},{"text":" has been found in the North-East!","color":"aqua"}]

setblock -625 70 119 minecraft:structure_block{name:"charitydigging:beacon",mode:"LOAD",posX:-2,posY:-3,posZ:-2}
setblock -625 71 119 minecraft:redstone_block
setblock -625 72 119 minecraft:red_stained_glass_pane