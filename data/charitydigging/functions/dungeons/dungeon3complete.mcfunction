advancement revoke @a only charitydigging:dungeon3
advancement grant @a only charitydigging:dungeon3

tellraw @p ["",{"text":"The ","color":"aqua"},{"text":"Blazing Abyss","color":"gold"},{"text":" has been conquered! It can now be removed.","color":"aqua"}]

title @a times 20 80 20
title @a title ["",{"text":"Dungeon cleared!","color":"red"}]
title @a subtitle ["",{"text":"The ","color":"aqua"},{"text":"Blazing Abyss","color":"gold"},{"text":" has been conquered!","color":"aqua"}]

fill -650 41 95 -603 28 142 minecraft:stone replace #charitydigging:dungeon3
fill -650 27 95 -603 14 142 minecraft:stone replace #charitydigging:dungeon3
fill -650 13 95 -603 5 142 minecraft:stone replace #charitydigging:dungeon3

fill -623 72 121 -627 67 117 air