#award points gained through breaking other blocks to the breakStone objective (with different values)
execute as @a if score @s breakAndesite matches 1.. run scoreboard players add @s breakStone 1
execute as @a if score @s breakAndesite matches 1.. run scoreboard players remove @s breakAndesite 1

execute as @a if score @s breakDiorite matches 1.. run scoreboard players add @s breakStone 1
execute as @a if score @s breakDiorite matches 1.. run scoreboard players remove @s breakDiorite 1

execute as @a if score @s breakGranite matches 1.. run scoreboard players add @s breakStone 1
execute as @a if score @s breakGranite matches 1.. run scoreboard players remove @s breakGranite 1

execute as @a if score @s breakCoal matches 1.. run scoreboard players add @s breakStone 2
execute as @a if score @s breakCoal matches 1.. run scoreboard players remove @s breakCoal 1

execute as @a if score @s breakIron matches 1.. run scoreboard players add @s breakStone 2
execute as @a if score @s breakIron matches 1.. run scoreboard players remove @s breakIron 1

execute as @a if score @s breakRedstone matches 1.. run scoreboard players add @s breakStone 3
execute as @a if score @s breakRedstone matches 1.. run scoreboard players remove @s breakRedstone 1

execute as @a if score @s breakGold matches 1.. run scoreboard players add @s breakStone 4
execute as @a if score @s breakGold matches 1.. run scoreboard players remove @s breakGold 1

execute as @a if score @s breakLapis matches 1.. run scoreboard players add @s breakStone 4
execute as @a if score @s breakLapis matches 1.. run scoreboard players remove @s breakLapis 1

execute as @a if score @s breakEmerald matches 1.. run scoreboard players add @s breakStone 4
execute as @a if score @s breakEmerald matches 1.. run scoreboard players remove @s breakEmerald 1

execute as @a if score @s breakDiamond matches 1.. run scoreboard players add @s breakStone 5
execute as @a if score @s breakDiamond matches 1.. run scoreboard players remove @s breakDiamond 1